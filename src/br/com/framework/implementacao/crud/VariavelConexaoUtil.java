package br.com.framework.implementacao.crud;


/**
 * Name path JNDI datasource Tomcat
 * @author Toda
 *
 */
public class VariavelConexaoUtil {

	public static String JAVA_COMP_ENV_JDBC_DATA_SOURCE = "java:/comp/env/jdbc/datasource";

}
