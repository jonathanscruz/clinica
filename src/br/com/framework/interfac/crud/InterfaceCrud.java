package br.com.framework.interfac.crud;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public interface InterfaceCrud<T> extends Serializable{

	// Save datas
	void save(T obj) throws Exception;
	
	// 
	void persist(T obj) throws Exception;
	
	// save or update
	void saveOrUpdate(T obj) throws Exception;
	
	//Update data
	void update(T obj) throws Exception;
	
	// delete data
	void delete(T obj) throws Exception;
	
	// save or update and return the object is persistent state
	T merge(T obj) throws Exception;
	
	// Load Class list data
	List<T> findList(Class<T> objs) throws Exception;
	
	
	Object findById(Class<T> entidade, Long id) throws Exception;
	
	T findByPorId(Class<T> entidade, Long id) throws Exception;
	
	List<T> findListByQueryDinamica(String s) throws Exception;
	
	
	//Execute update with HQL
	void executeUpdateQueryDinamica(String s) throws Exception;
	
	// execute UPDATE with SQL
	void executeUpdateSQLDinamica(String s) throws Exception;
	
	//Clean session Hibernate
	void clearSession() throws Exception;
	
	// Remove object in hibernate session
	void evict(Object objs) throws Exception;

	Session getSession() throws Exception;
	
	List<?> getListSQLDinamica(String sql) throws Exception;
	
	// JDBC Spring
	JdbcTemplate getJdbcTemplate();
	
	SimpleJdbcTemplate getSimpleJdbcTemplate();
	
	SimpleJdbcInsert getSimpleJdbcInsert();
	
	Long totalRegistro(String table) throws Exception;
	
	Query obterQuery(String query) throws Exception;
	
	
	// Loading dynamic with JSF and Primefaces
	List<T> findListByQueryDinamica(String query, int iniciaNoRegistro, int maximoResultado) throws Exception;
}
